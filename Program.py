from flask import Flask, render_template, request, jsonify
import urllib.request
import json
import os
import ssl

app = Flask(__name__)

@app.route('/')
def index(dynamic_song_info = "Your song", dynamic_song_placement = "Song placement"):
    return render_template('index.html', dynamic_song_info=dynamic_song_info, dynamic_song_placement=dynamic_song_placement)


@app.route('/submit_form', methods=['POST'])
def submit_form():

    formData = request.get_json()
    print(f'Request form: {formData}')
    data = transform_data(formData)
    
    response = sendData(data)
    track_name = data["input1"][0]["track_name"]
    artist_name = data["input1"][0]["artist(s)_name"]
    songInfoText = str(f'{track_name} by {artist_name}')
    return jsonify({"status": "success", "dynamic_song_placement" : str(response), "dynamic_song_info" : songInfoText})

def transform_data(originalData):
    
    print("TRACK NAME: ")
    print(originalData["track_name"])

    release_date_str = originalData.get("releaseDate")
    print(release_date_str)

    if release_date_str:
        released_year = int(release_date_str.split('-')[0])
        released_month = int(release_date_str.split('-')[1])
        released_day = int(release_date_str.split('-')[2])
    else:
        released_year = 2020 
        released_month = released_day = 1

    transformedData = {
        "input1": [
            {
                "track_name": originalData.get("track_name") if originalData.get("track_name") else "No Song",
                "artist(s)_name": originalData.get("artist(s)_name") if originalData.get("artist(s)_name") else "No Artist",
                "released_year": released_year,
                "released_month": released_month,
                "released_day": released_day,
                "in_spotify_playlists": int(originalData.get("in_spotify_playlists")) if originalData.get("in_spotify_playlists") else 0,
                "in_apple_playlists": int(originalData.get("in_apple_playlists")) if originalData.get("in_apple_playlists") else 0,
                "in_apple_charts": int(originalData.get("in_apple_charts")) if originalData.get("in_apple_charts") else 0,
                "in_deezer_charts": int(originalData.get("in_deezer_charts")) if originalData.get("in_deezer_charts") else 0,
                "bpm": int(originalData.get("bpm")) if originalData.get("bpm") else 0,
                "key": originalData.get("key") if originalData.get("key") else "A",
                "danceability_%": int(originalData.get("danceability_%")) if originalData.get("danceability_%") else 0,
                "valence_%": int(originalData.get("valence_%")) if originalData.get("valence_%") else 0,
                "energy_%": int(originalData.get("energy_%")) if originalData.get("energy_%") else 0,
                "liveness_%": int(originalData.get("liveness_%")) if originalData.get("liveness_%") else 0,
                "speechiness_%": int(originalData.get("speechiness_%")) if originalData.get("speechiness_%") else 0,
            }
        ]
    }

    print(f'Transformed data: {transformedData}')
    return transformedData

def allowSelfSignedHttps(allowed):
    if allowed and not os.environ.get('PYTHONHTTPSVERIFY', '') and getattr(ssl, '_create_unverified_context', None):
        ssl._create_default_https_context = ssl._create_unverified_context

def sendData(data):
    allowSelfSignedHttps(True)

    data =  {
    "Inputs": data,
    "GlobalParameters": {}
    }

    body = str.encode(json.dumps(data))

    url = 'http://59175a28-b7e7-4f60-ab7b-776f0c8418e0.westeurope.azurecontainer.io/score'
    # Replace this with the primary/secondary key or AMLToken for the endpoint
    api_key = 'xupe0A80pfH3bkc5dI1XzIoskmHGou8u'
    if not api_key:
        raise Exception("A key should be provided to invoke the endpoint")


    headers = {'Content-Type':'application/json', 'Authorization':('Bearer '+ api_key)}

    req = urllib.request.Request(url, body, headers)

    try:
        response = urllib.request.urlopen(req)

        result = response.read()
        result_str = result.decode('utf-8')
        parsed_json = json.loads(result_str)
        predicted_value = parsed_json["Results"]["WebServiceOutput0"][0]["predicted_in_spotify_charts"]
        prediction = int(predicted_value)
        print(f'Prediction: {prediction}')
        return prediction
    
    except urllib.error.HTTPError as error:
        print("The request failed with status code: " + str(error.code))

        # Print the headers - they include the requert ID and the timestamp, which are useful for debugging the failure
        print(error.info())
        print(error.read().decode("utf8", 'ignore'))

@app.route('/get_dynamic_song_info', methods=['GET'])
def displaySongInfo():
    dynamic_song_info = "Dynamic Song Info"
    dynamic_song_placement = "Dynamic Song Placement"
    return jsonify({'dynamic_song_info': dynamic_song_info, 'dynamic_song_placement': dynamic_song_placement})


if __name__ == '__main__':
    app.run(debug=True)